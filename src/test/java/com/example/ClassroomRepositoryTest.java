package com.example;

import com.example.model.Classroom;
import com.example.model.User;
import com.example.repository.ClassroomRepository;
import com.example.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ClassroomRepositoryTest {
    @Qualifier("classroomRepository")
    @Autowired
    private ClassroomRepository classroomRepository;

    @Test
    public void findByNumberTest() {
        Classroom classroom = classroomRepository.findByNumber(1301);
        assertEquals(java.util.Optional.of(1301), classroom.getNumber());
    }



}
