package com.example.service.implementation;

import com.example.model.Booking;
import com.example.model.Classroom;
import com.example.model.Record;
import com.example.model.User;
import com.example.repository.*;
import com.example.service.BookingService;
import com.example.service.ClassroomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

@Service
public class BookingServiceImpl implements BookingService {
    @Qualifier("classroomRepository")
    @Autowired
    ClassroomRepository classroomRepository;

    @Autowired
    ScheduleRepository scheduleRepository;

    @Autowired
    BookingRepository bookingRepository;

    @Autowired
    RecordRepository recordRepository;

    @Autowired
    BookingService bookingService;

    @Qualifier("userRepository")
    @Autowired
    UserRepository userRepository;

    @Override
    public int findSuitableClassroom(int lesson, int numberOfPersons) {
        List suitableClassrooms = bookingRepository.findSuitableClassroom(lesson, numberOfPersons);
        return (int) suitableClassrooms.get(0);
    }

    @Override
    public void saveBooking(String userEmail, int classroomNumber, int lesson) {
        User user = userRepository.findByEmail(userEmail);
        Classroom classroom = classroomRepository.findByNumber(classroomNumber);

        Booking booking = new Booking(classroom, user, lesson);
        bookingRepository.save(booking);

        Record record = new Record(user, classroom, false);
        recordRepository.save(record);
    }



}
