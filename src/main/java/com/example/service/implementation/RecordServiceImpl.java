package com.example.service.implementation;


import com.example.model.Record;
import com.example.model.Role;
import com.example.model.User;
import com.example.repository.RecordRepository;
import com.example.repository.UserRepository;
import com.example.service.RecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

@Service
public class RecordServiceImpl implements RecordService {
    @Autowired
    RecordRepository recordRepository;

    @Qualifier("userRepository")
    @Autowired
    UserRepository userRepository;

    @Override
    public void saveRecord(Record record) {
        recordRepository.save(record);
    }

    @Override
    public List<Record> getUserRecords(String userEmail) {
        User user = userRepository.findByEmail(userEmail);
        return recordRepository.findAllByUser(user);
    }

    @Override
    public Record findById(Integer id) {
        return recordRepository.findById(id);
    }

}
