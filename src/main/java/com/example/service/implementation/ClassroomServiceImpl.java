package com.example.service.implementation;


import com.example.model.Classroom;
import com.example.repository.ClassroomRepository;
import com.example.service.ClassroomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("classroomService")
public class ClassroomServiceImpl implements ClassroomService {
    @Qualifier("classroomRepository")
    @Autowired
    private ClassroomRepository classroomRepository;

    @Override
    public Classroom findClassroomByNumber(Integer number) {
        return classroomRepository.findByNumber(number);
    }

    @Override
    public List<Classroom> findAllByOrderByNumberAsc() {
        return classroomRepository.findAllByOrderByNumberAsc();
    }


}
