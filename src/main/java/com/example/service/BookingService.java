package com.example.service;

import com.example.model.Booking;

public interface BookingService {
    int findSuitableClassroom(int lesson, int numberOfPersons);
    void saveBooking(String userEmail, int classroomNumber, int lesson);
}
