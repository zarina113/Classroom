package com.example.service;

import com.example.model.Classroom;

import java.util.List;

public interface ClassroomService {
    Classroom findClassroomByNumber(Integer number);

    List<Classroom> findAllByOrderByNumberAsc();

}
