package com.example.service;

import com.example.model.Record;

import java.util.List;

public interface RecordService {
    void saveRecord(Record record);
    List<Record> getUserRecords(String userEmail);
    Record findById(Integer id);
}
