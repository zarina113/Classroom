package com.example.repository;

import com.example.model.Booking;
import com.example.model.Classroom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookingRepository extends JpaRepository<Booking, Integer> {
    @Query(value = "select classroom_id from bookings WHERE classroom_id=?1", nativeQuery = true)
    int findByClassroomId(int classroomId);


    @Query(value = "SELECT number FROM(SELECT number, capacity FROM bookings b JOIN classroom c USING(classroom_id) " +
            "WHERE b.lesson<>?1 UNION SELECT number, capacity FROM classroom c LEFT JOIN bookings b USING(classroom_id) " +
            "WHERE b.classroom_id is NULL) results WHERE capacity>=?2 ORDER BY capacity", nativeQuery = true)
    List findSuitableClassroom(int lesson, int numberOfPersons);


}