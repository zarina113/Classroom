package com.example.repository;

import com.example.model.Classroom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("classroomRepository")
public interface ClassroomRepository extends JpaRepository<Classroom, Integer> {
    //@Query(value = "select * from Classroom where number=?1", nativeQuery = true)
    Classroom findByNumber(Integer number);
/*
    @Query(value = "select * from Classroom  where hasConditioner=TRUE and " +
                                                    "hasProjector=TRUE and " +
                                                    "capacity>=?1 and free=TRUE", nativeQuery = true)*/


    List<Classroom> findAllByOrderByNumberAsc();



}
