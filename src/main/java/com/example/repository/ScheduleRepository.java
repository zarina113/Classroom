package com.example.repository;

import com.example.model.Classroom;
import com.example.model.Schedule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ScheduleRepository extends JpaRepository<Schedule, Integer> {
    @Query(value = "select classroom_id from schedule WHERE " +
            "day_of_week=?1 AND lesson=?2 AND classroom_id=?3", nativeQuery = true)
    List<Integer> findByDayOfWeekAndAndLessonAndClassroomId(int dayOfWeek, int lesson, int classroomId);
}
