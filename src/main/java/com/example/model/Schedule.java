package com.example.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "schedule")
public class Schedule {
    @Id
    @Column(name = "id")
    @GeneratedValue
    private int id;

    @Column(name = "day_of_week")
    private int dayOfWeek;

    @Column(name = "lesson")
    private int lesson;

/*
    @ManyToMany
    @JoinColumn(name = "classroom_id")
    private int classroomId;
*/
}
