package com.example.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "record")
public class Record {
    @Id
    @Column(name = "record_id")
    @GeneratedValue
    private Integer id;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

    @OneToOne
    @JoinColumn(name = "classroom_id")
    private Classroom classroom;

    @Column(name = "closed")
    private Boolean closed;

    @Column(name = "date")
    private Date date;

    public Record(User user, Classroom classroom, Boolean closed) {
        this.user = user;
        this.closed = closed;
        this.classroom = classroom;
        this.date = new Date();
    }


}
