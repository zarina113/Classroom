package com.example.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "bookings")
public class Booking {
    @Id
    @Column(name = "id")
    @GeneratedValue
    private int id;

    @OneToOne
    @JoinColumn(name = "classroom_id")
    private Classroom classroom;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;
    
    @Column(name = "lesson")
    private int lesson;

    public Booking(Classroom classroom, User user, int lesson) {
        this.classroom = classroom;
        this.user = user;
        this.lesson = lesson;
    }
}
