package com.example.controller;

import com.example.model.Classroom;
import com.example.model.Record;
import com.example.service.ClassroomService;
import com.example.service.RecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
public class MainController {
    @Autowired
    private ClassroomService classroomService;

    @GetMapping("/main")
    public ModelAndView home(){
        ModelAndView modelAndView = new ModelAndView();
        List<Classroom> classrooms = classroomService.findAllByOrderByNumberAsc();
        modelAndView.addObject("classrooms", classrooms);
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        Date date = new Date();
        modelAndView.addObject("date", "Audiences for the current time: " + date);
        modelAndView.setViewName("main");
        return modelAndView;
    }

}
