package com.example.controller;

import com.example.model.Classroom;
import com.example.model.Record;
import com.example.service.BookingService;
import com.example.service.ClassroomService;
import com.example.service.RecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@Controller
public class BookingController {
    @Autowired
    private BookingService bookingService;

    @Autowired
    private ClassroomService classroomService;

    @GetMapping("/booking")
    public ModelAndView booking() {
        ModelAndView modelAndView = new ModelAndView();
        Record record = new Record();
        modelAndView.addObject("record", record);
        modelAndView.setViewName("booking");
        return modelAndView;
    }

    @PostMapping("/booking")
    public ModelAndView createNewBooking(Principal principal,
                                         @RequestParam("lesson") int lesson,
                                         @RequestParam("numberOfPersons") int numberOfPersons) {

        Integer classroomNumber = bookingService.findSuitableClassroom(lesson, numberOfPersons);
        ModelAndView modelAndView = new ModelAndView();

        if (classroomNumber == null) {
            modelAndView.addObject("result", "Sorry, either all the classrooms are busy, " +
                    "or there is not a suitable classroom for your number of persons");
        }
        else {
            bookingService.saveBooking(principal.getName(), classroomNumber, lesson);

            modelAndView.addObject("result", "Your classroom is " + classroomNumber);
        }

        modelAndView.setViewName("booking_result");
        return modelAndView;
    }
}
