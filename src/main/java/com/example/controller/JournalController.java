package com.example.controller;

import com.example.model.Record;
import com.example.service.BookingService;
import com.example.service.RecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.thymeleaf.expression.Lists;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Controller
public class JournalController {
    @Autowired
    private RecordService recordService;

    @GetMapping("/journal")
    public ModelAndView journal(Model model, Principal principal) {
        ModelAndView modelAndView = new ModelAndView();

        List<Record> records = recordService.getUserRecords(principal.getName());
        Collections.reverse(records);


        modelAndView.addObject("records", records);
        modelAndView.setViewName("journal");
        return modelAndView;
    }

    @PostMapping("/close/{recordId}")
    public String close(@PathVariable("recordId") Integer recordId) {
        Record record = recordService.findById(recordId);
        record.setClosed(true);
        recordService.saveRecord(record);
        return "redirect:/journal";
    }

}
